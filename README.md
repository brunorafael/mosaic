## Symfony 4.4 - skeleton

#### Requirements:
- docker

##### APP 
- annotations
- security
- form
- validator
- orm

##### DEV 
- phpunit-bridge
- phpunit
- profiler (frontend)
- maker
- orm-fixtures
- liip/functional-test-bundle

#### CUSTOM 
- lexik/jwt-authentication-bundle
- nelmio/cors-bundle
- jms/serializer-bundle
- friendsofsymfony/rest-bundle

#### Webpack 
- encore 
- asset